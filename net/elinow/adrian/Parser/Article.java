package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Article extends PartOfSpeech{
    public Article(String value){
        this.setValue(value);
    }
    public String whatami(){return "Article";}
    public boolean equals(Object o){
        Article incoming = (Article) o;
        return incoming.getValue().equals(this.getValue());
    }
}
