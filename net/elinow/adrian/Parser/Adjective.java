package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Adjective extends PartOfSpeech {
    public Adjective(String value){
        this.setValue(value);
    }

    public String whatami(){return "Adjective";}
    public boolean equals(Object o){
        Adjective incoming = (Adjective) o;
        return incoming.getValue().equals(this.getValue());
    }
}
