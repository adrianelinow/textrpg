package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Conjunction extends PartOfSpeech {
    public Conjunction(String value){
        this.setValue(value);
    }

    public String whatami(){return "Conjunction";}

    public boolean equals(Object o){
        Conjunction incoming = (Conjunction) o;
        return incoming.getValue().equals(this.getValue());
    }
}
