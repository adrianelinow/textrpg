package net.elinow.adrian.Parser;

import java.util.HashMap;
import java.util.ArrayList;
/**
 * Created by adrian on 5/7/17.
 */
public class ParserTester {

    public static void main(String[] args){

        Parser parser = new Parser();

        HashMap<String, ArrayList<String>> parserTests = new HashMap<String, ArrayList<String>>(){{
            ///*
            put("test051", new ArrayList<String>(){{
                add("get rock,pliers, hammer and squirrel and hammer squirrel into the wall.");
            }});
            put("test050", new ArrayList<String>(){{
                add("get hammer and squirrel from Bob and then hammer squirrel into the wall.");
                add("get the big, heavy hammer and kill Bob with it!");
                add("get the large, gold brick.");
                add("get the large gold brick.");
                add("get the gold gold");
                add("get gold");
                add("paint the bucket gold");
                add("paint the bucket gold.");
                add("paint the gold bucket black");
                add("paint the gold bucket black!");
                add("kill elf, get gold");
                add("kill elf and get gold");
            }});
            put("test052", new ArrayList<String>(){{
                add("kill the trite little elf with my sword.");
                add("kill the trite little elf with my sword, then wipe the blood off of my sword!");
                add("kill the trite little elf with my sword, then wipe the blood off of it!");
                add("destroy the cantankerous creature then eat the dessert");
            }});
            //*/
            put("ongoing", new ArrayList<String>(){{
                add("walk down the hall, pry open the door, and enter the room");
            }});
        }};

        for(String section : parserTests.keySet()){
            System.out.println("Testing set:\t"+section);
            for(String test : parserTests.get(section)) {
                ArrayList<PartOfSpeech> result = parser.tokenize(test);
                for (PartOfSpeech word : result) {
                    System.out.print(word.whatami() + " ");
                }
                System.out.println("\n########-########\n");
                ArrayList<ArrayList<PartOfSpeech>> grouped = parser.groupStatements(result);
                for (ArrayList<PartOfSpeech> statement : grouped) {
                    System.out.println("");
                    for(PartOfSpeech pos : statement){
                        System.out.print(pos.getValue()+" ");
                    }
                    System.out.print(" | ");
                }
                System.out.println("\n\n===============================\n\n");
            }
        }
    }
}
