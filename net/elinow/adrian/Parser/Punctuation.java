package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Punctuation extends PartOfSpeech {
    public Punctuation(String value){
        this.setValue(value);
    }

    public String whatami(){return "Punctuation";}

    public boolean equals(Object o){
        Punctuation incoming = (Punctuation) o;
        return incoming.getValue().equals(this.getValue());
    }
}