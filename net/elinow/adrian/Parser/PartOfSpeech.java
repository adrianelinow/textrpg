package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public abstract class PartOfSpeech {

    private String value;

    public PartOfSpeech(String value){this.value = value;}
    public PartOfSpeech(){}

    public String getValue(){ return value; }

    public void setValue(String newValue) {
        try {
            value = newValue;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract String whatami();
}
