package net.elinow.adrian.Parser;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by adrian on 5/3/17.
 */
public class Parser {

    private HashMap<String, Object[]> ObjectLookup = new HashMap<>();
    private HashMap<String, ArrayList<String>> POSLookup = new HashMap<>();

    private HashMap<Character, Character> scrubber = new HashMap<>();

    // Part of Speech Arrays
    private ArrayList<Verb> verbs = new ArrayList<Verb>();
    private HashMap<Verb, ArrayList<Verb>> verbSynonyms = new HashMap<Verb, ArrayList<Verb>>();
    private ArrayList<Conjunction> conjunctions = new ArrayList<Conjunction>();
    private ArrayList<Preposition> prepositions = new ArrayList<Preposition>();
    private ArrayList<Article> articles = new ArrayList<Article>();
    private ArrayList<Pronoun> pronouns = new ArrayList<Pronoun>();
    private ArrayList<Punctuation> punctuations = new ArrayList<Punctuation>();
    private ArrayList<ProperNoun> properNouns = new ArrayList<ProperNoun>();
    private ArrayList<Adjective> adjectives = new ArrayList<Adjective>();
    private ArrayList<ArrayList<Noun>> lastNouns = new ArrayList<ArrayList<Noun>>();

    private ArrayList<ArrayList<PartOfSpeech>> history = new ArrayList<ArrayList<PartOfSpeech>>();

    /*
     * Constructor
     */
    public Parser()
    {
        loadConfiguration();
        setConfiguration();
        Populate();
    }

    /*
     * Cleans up input by converting to lower case,
     *  stripping spaces, and making punctuation easier
     *  to read
     */
    public String scrub(String toScrub){
        toScrub = toScrub.replaceAll(", ", " , ");
        toScrub = toScrub.replace("."," . ");
        return toScrub.toLowerCase().trim();
    }

    public ArrayList<ArrayList<PartOfSpeech>> groupStatements(ArrayList<PartOfSpeech> classified){

        ArrayList<Integer> verbBreakIndexes = new ArrayList<Integer>();

        for(int idx = 0; idx < classified.size(); idx++){
            if(classified.get(idx) instanceof Verb){
                verbBreakIndexes.add(idx);
            }
        }

        ArrayList<ArrayList<PartOfSpeech>> statements = new ArrayList<ArrayList<PartOfSpeech>>();

        int firstVerb = 0, lastVerb = verbBreakIndexes.size()-1;

        for(int i = firstVerb; i < verbBreakIndexes.size(); i++){
            ArrayList<PartOfSpeech> current = new ArrayList<PartOfSpeech>();
            if(i != lastVerb){
                for(int j = verbBreakIndexes.get(i); j < verbBreakIndexes.get(i+1); j++){
                    current.add(classified.get(j));
                }
            }else{
                for(int j = verbBreakIndexes.get(i); j < classified.size(); j++){
                    current.add(classified.get(j));
                }
            }
            statements.add(current);
        }
        for(int is = 0; is < statements.size(); is++){
            for(int i = statements.get(is).size()-2; i > 0; i--){
                if(statements.get(is).get(i) instanceof Punctuation){
                    statements.get(is).remove(i);
                }
                if(conjunctions.contains(new Conjunction(statements.get(is).get(i).getValue()))){
                    statements.get(is).remove(i);
                }
            }
        }

        for(ArrayList<PartOfSpeech> statement : statements){
            updateHistory(statement);
            ArrayList<Noun> nounList = new ArrayList<Noun>();
            for(PartOfSpeech word : statement)
                if(word instanceof Noun)
                    nounList.add(new Noun(word.getValue()));
            logLastNouns(nounList);
        }
        return statements;
    }

    private void logLastNouns(ArrayList<Noun> nounsList){
        lastNouns.add(nounsList);
        if(lastNouns.size() > 3)
            lastNouns.remove(0);
    }

    public String getMostRecentCommand(){
        if(history.size() > 0){
            String ret = "";
            for(PartOfSpeech pos : history.get(history.size()-1))
                ret += pos.getValue()+" ";
            return ret;
        }
        return "";
    }

    private void updateHistory(ArrayList<PartOfSpeech> statement){
        history.add(statement);
        if(history.size() > 25)
            history.remove(0);
    }

    /*
     * Dictates the part of speech of the input line
     *  and returns them in their respective types
     */

    public ArrayList<PartOfSpeech> tokenize(String toParse) {
        if (!toParse.isEmpty() || !toParse.equals("")) {
            toParse = scrub(toParse);
            System.out.println(toParse);
            ArrayList<PartOfSpeech> classified = new ArrayList<>();
            String[] unclassified = toParse.split(" ");
            for (String word : unclassified) {
                if (properNouns.contains(new ProperNoun(word))) {
                    classified.add(new ProperNoun(word));
                }else if (adjectives.contains(new Adjective(word))) {
                    classified.add(new Adjective(word));
                } else if (pronouns.contains(new Pronoun(word))) {
                    classified.add(new Pronoun(word));
                } else if (verbs.contains(new Verb(word))) {
                    classified.add(new Verb(word));
                } else if (prepositions.contains(new Preposition(word))) {
                    classified.add(new Preposition(word));
                } else if (articles.contains(new Article(word))) {
                    classified.add(new Article(word));
                } else if (conjunctions.contains(new Conjunction(word))) {
                    classified.add(new Conjunction(word));
                } else if (punctuations.contains(new Punctuation(word))) {
                    classified.add(new Punctuation(word));
                }else{
                    classified.add(new Noun(word));
                }
            }

            int first = 0;
            int last;
            if(classified.size()-1 <= first)
                last = first;
            else
                last = classified.size()-1;


            for (int e = 0; e < classified.size(); e++)
                if (classified.get(e) instanceof Noun)
                    if (e != last) {
                        if (classified.get(e + 1) instanceof Noun)
                            classified.set(e, new Verb(classified.get(e).getValue()));
                    }else if (e == first){
                        classified.set(e, new Verb(classified.get(e).getValue()));
                    }
            for (int e = 0; e < classified.size(); e++)
                if (classified.get(e) instanceof Noun)
                    if (e < last)
                        if (classified.get(e + 1) instanceof ProperNoun)
                            classified.set(e, new Verb(classified.get(e).getValue()));

            for (int e = 0; e < classified.size(); e++)
                if (classified.get(e) instanceof Verb)
                    if (e < last && e > first)
                        if (classified.get(e+1) instanceof Verb){
                            classified.set(e+1, new Noun(classified.get(e+1).getValue()));
                    }
            for (int e = 0; e < classified.size(); e++)
                if (classified.get(e) instanceof Verb)
                    if (e < last && e > first){
                        if (!(classified.get(e+1) instanceof Noun &&
                            !(classified.get(e+1) instanceof ProperNoun))){
                            if(!(classified.get(e-1) instanceof Punctuation &&
                               !(classified.get(e-1) instanceof Conjunction))) {
                                classified.set(e, new Noun(classified.get(e).getValue()));
                            }
                        }
                    }
            for (int e = 0; e < classified.size(); e++) {
                if (classified.get(e) instanceof Adjective) {
                    if (e == last &&
                            !(classified.get(e - 1) instanceof Noun) &&
                            !(classified.get(e - 1) instanceof Conjunction) &&
                            !(classified.get(e - 1) instanceof Punctuation)) {
                        classified.set(e, new Noun(classified.get(e).getValue()));
                    }else if (e == first) {
                        classified.set(e, new Verb(classified.get(e).getValue()));
                    } else {
                        if (classified.get(e - 1) instanceof Verb)
                            classified.set(e, new Noun(classified.get(e).getValue()));
                    }
                }
            }
            if (last > first){
                if (classified.get(last) instanceof Noun)
                    if (classified.get(last - 1) instanceof Noun)
                        classified.set(last, new Adjective(classified.get(last).getValue()));
                if (classified.get(last) instanceof Verb)
                    if(classified.get(last-1) instanceof Adjective ||
                       classified.get(last-1) instanceof Article)
                        classified.set(last, new Noun(classified.get(last).getValue()));
            }
            for (int e = 0; e < classified.size(); e++) {
                if (first < e && e < last)
                    if (classified.get(e) instanceof Verb) {
                        if (classified.get(e) instanceof Verb &&
                                classified.get(e + 1) instanceof Preposition &&
                                (classified.get(e - 1) instanceof Conjunction ||
                                        classified.get(e - 1) instanceof Punctuation))
                            classified.set(e, new Noun(classified.get(e).getValue()));
                        else if (classified.get(e) instanceof Verb &&
                                !(classified.get(e + 1) instanceof Punctuation ||
                                        classified.get(e+1) instanceof Conjunction) &&
                                        (classified.get(e + 1) instanceof Conjunction ||
                                        classified.get(e - 1) instanceof Punctuation))
                            classified.set(e, new Noun(classified.get(e).getValue()));
                        else if (classified.get(e) instanceof Verb &&
                                classified.get(e - 1) instanceof Article &&
                                classified.get(e + 1) instanceof Noun) {
                            classified.set(e, new Noun(classified.get(e).getValue()));
                            classified.set(e + 1, new Preposition(classified.get(e+1).getValue()));
                        } else if (classified.get(e) instanceof Verb &&
                                classified.get(e - 1) instanceof Pronoun &&
                                classified.get(e + 1) instanceof Noun)
                            classified.set(e, new Adjective(classified.get(e).getValue()));
                    }
                    if(classified.get(e) instanceof Noun){
                        if(e < last && classified.get(e-1) instanceof Pronoun &&
                                classified.get(e+1) instanceof Pronoun)
                            classified.set(e, new Verb(classified.get(e).getValue()));
                        if(classified.get(e) instanceof Noun && (e < last) &&
                                classified.get(e+1) instanceof Article)
                            if(prepositions.contains(classified.get(e).getValue()))
                                classified.set(e, new Preposition(classified.get(e).getValue()));
                        if(classified.get(e-1) instanceof Conjunction &&
                           classified.get(e+1) instanceof Article){
                            classified.set(e, new Verb(classified.get(e).getValue()));
                        }
                        if(classified.get(e-1) instanceof Punctuation &&
                            classified.get(e+1) instanceof Noun){
                            classified.set(e, new Verb(classified.get(e).getValue()));
                        }
                        if(e < last && classified.get(e+1) instanceof Preposition &&
                                (classified.get(e-1) instanceof Conjunction ||
                                classified.get(e-1) instanceof Punctuation)){
                            classified.set(e, new Verb(classified.get(e).getValue()));
                        }
                        if( e < last && classified.get(e-1) instanceof Verb &&
                                (classified.get(e+1) instanceof Noun ||
                                classified.get(e+1) instanceof Pronoun))
                            if(prepositions.contains(classified.get(e).getValue()))
                                classified.set(e, new Preposition(classified.get(e).getValue()));
                    }
                    if(classified.get(e) instanceof Article)
                        if(classified.get(e-1) instanceof ProperNoun &&
                                (classified.get(e+1) instanceof Noun))
                            if(verbs.contains(classified.get(e-1).getValue()))
                                classified.set(e-1, new Verb(classified.get(e-1).getValue()));
            }
            return classified;
        }
        return null;
    }

    /*
     * Converts each part of speech array in ObjectLookup to the
     *      respective type in POSLookup for population of Arrays
     */
    public void setConfiguration(){
        for(String pos : ObjectLookup.keySet()){
            System.out.print("Parser Loading:" + pos+"\n");
            POSLookup.put(pos, getConfiguration(pos));
            //System.out.print("\t[Loaded Successfully]\n");
        }
    }

    /*
     * Converts from Object to String and returns full Array of Strings
     */
    public ArrayList<String> getConfiguration(String pos){
        ArrayList<String> config = new ArrayList<String>();
        for(Object o : ObjectLookup.get(pos)){
            config.add(o.toString());
        }
        return config;
    }

    /*
    Imports text files of Parts of Speech
        for transference from types Object[] to String[]
     */
    private void loadConfiguration() {
        try {
            ObjectLookup.put("verbs", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/verbs.txt"))).lines().toArray());
            ObjectLookup.put("punctuations", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/punctuations.txt"))).lines().toArray());
            ObjectLookup.put("pronouns", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/pronouns.txt"))).lines().toArray());
            ObjectLookup.put("prepositions", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/prepositions.txt"))).lines().toArray());
            ObjectLookup.put("conjunctions", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/conjunctions.txt"))).lines().toArray());
            ObjectLookup.put("articles", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/articles.txt"))).lines().toArray());
            ObjectLookup.put("adjectives", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/adjectives.txt"))).lines().toArray());
            // Synonyms //
            ObjectLookup.put("synonyms", new BufferedReader(new FileReader(
                    new File("src/net/elinow/adrian/Parser/TextFiles/synonyms.txt"))).lines().toArray());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*
     * Populates Part of Speech Arrays with
     *     values loaded from text files to POSLookup
     */
    private void Populate(){
        for(String s : POSLookup.get("verbs"))
            verbs.add(new Verb(s));
        for(String s : POSLookup.get("conjunctions"))
            conjunctions.add(new Conjunction(s));
        for(String s : POSLookup.get("punctuations"))
            punctuations.add(new Punctuation(s));
        for(String s : POSLookup.get("pronouns"))
            pronouns.add(new Pronoun(s));
        for(String s : POSLookup.get("prepositions"))
            prepositions.add(new Preposition(s));
        for(String s : POSLookup.get("conjunctions"))
            conjunctions.add(new Conjunction(s));
        for(String s : POSLookup.get("articles"))
            articles.add(new Article(s));
        for(String s : POSLookup.get("adjectives"))
            adjectives.add(new Adjective(s));


    }

}