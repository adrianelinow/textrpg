package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Preposition extends PartOfSpeech {
    public Preposition(String value){
        this.setValue(value);
    }

    public String whatami(){return "Preposition";}

    public boolean equals(Object o){
        Preposition incoming = (Preposition) o;
        return incoming.getValue().equals(this.getValue());
    }
}
