package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Verb extends PartOfSpeech{
    public Verb(String value){
        this.setValue(value);
    }

    public String whatami(){return "Verb";}

    public boolean equals(Object o){
        Verb incoming = (Verb) o;
        return incoming.getValue().equals(this.getValue());
    }
}
