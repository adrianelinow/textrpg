package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class ProperNoun extends Noun{
    public ProperNoun(String value){
        super(value);
    }

    public String whatami(){return "ProperNoun";}

    public boolean equals(Object o){
        ProperNoun incoming = (ProperNoun) o;
        return incoming.getValue().equals(this.getValue());
    }
}
