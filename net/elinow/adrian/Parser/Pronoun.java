package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Pronoun extends PartOfSpeech{
    public Pronoun(String value) {
        this.setValue(value);

    }
    public String whatami(){return "Pronoun";}
    public boolean equals(Object o){
        Pronoun incoming = (Pronoun) o;
        return incoming.getValue().equals(this.getValue());
    }
}
