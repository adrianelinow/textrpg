package net.elinow.adrian.Parser;

/**
 * Created by adrian on 5/5/17.
 */
public class Noun extends PartOfSpeech{
    public Noun(String value){
        this.setValue(value);
    }
    public String whatami(){return "Noun";}

    public boolean equals(Object o){
        Noun incoming = (Noun) o;
        return incoming.getValue().equals(this.getValue());
    }
}
