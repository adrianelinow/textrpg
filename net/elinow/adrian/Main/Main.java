package net.elinow.adrian.Main;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import net.elinow.adrian.World.*;
import net.elinow.adrian.Parser.Parser;
import net.elinow.adrian.Parser.PartOfSpeech;

public class Main extends Application {

    public final static int screenWidth = 1024,
            screenHeight = 640,
            consoleWidth = (int) (screenWidth * .75);
    public GraphicsContext gc;
    public TextArea logScreen;
    public Parser parser;

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            primaryStage.setTitle("The Scourge of the Lothreshire");

            // SETUP
            Group root = new Group();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            Canvas canvas = new Canvas(screenWidth, screenHeight);
            primaryStage.setResizable(false);
            primaryStage.setWidth(screenWidth);
            primaryStage.setHeight(screenHeight);
            root.getChildren().add(canvas);
            gc = canvas.getGraphicsContext2D();

            // World, Parser, and Interpreter initializations //
            Loader loader = new Loader("src/net/elinow/adrian/notes/World.json");
            World world = loader.load();
            world.printSelf();
            parser = new Parser();
            Interpreter interpreter = new Interpreter(world, parser);

           // SimulaeObjectFactory factory = new SimulaeObjectFactory();

            // Interface Elements
            logScreen = new TextArea();
            logScreen.setLayoutX(16);
            logScreen.setLayoutY(16);
            logScreen.setPrefSize(640, 560);
            logScreen.setEditable(false);
            logScreen.setWrapText(true);
            logScreen.appendText("Welcome to 'The Scourge of Lothreshire'\n\n");
            root.getChildren().add(logScreen);

            TextField inputField = new TextField();
            inputField.setLayoutX(0);
            inputField.setLayoutY(screenHeight - 56);
            inputField.setPrefSize(consoleWidth, 10);
            inputField.setPromptText("[Enter Command Here]");
            inputField.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    logScreen.appendText(">>>"+inputField.getText()+"\n");
                    logScreen.appendText(interpreter.parse(inputField.getText())+"\n");
                    inputField.setText("");
                }
            });
            inputField.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if(event.isShiftDown()){

                    }
                }
            });
            root.getChildren().add(inputField);

            // Assets
            Image consoleBackground = new Image("net/elinow/adrian/iu.jpeg");
            Image inventoryBackground = new Image("net/elinow/adrian/parchment.jpg");

            gc.setStroke(Color.BLACK);
            gc.setLineWidth(4);

            new AnimationTimer() {
                @Override
                public void handle(long currentNanoTime) {
                    // DRAW HERE

                    gc.drawImage(consoleBackground, 0, 0, consoleWidth, screenHeight);
                    gc.drawImage(inventoryBackground, consoleWidth, 0, screenWidth, screenHeight);
                    gc.strokeLine(consoleWidth, 0, consoleWidth, screenHeight);

                    scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            //CLICK EVENTS HERE

                        }
                    });

                }
            }.start();
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String group(ArrayList<PartOfSpeech> result){
        ArrayList<ArrayList<PartOfSpeech>> grouped = parser.groupStatements(result);
        String grouping = "";
        for(ArrayList<PartOfSpeech> list : grouped){
            grouping += "\n";
            for(PartOfSpeech pos : list){
                grouping += " - "+pos.getValue()+"("+pos.whatami()+")";
            }
        }
        return grouping;
    }

    public String Parse(String line){
        ArrayList<PartOfSpeech> result = parser.tokenize(line);
        String layout = "";
        if(result != null) {
            for (PartOfSpeech word : result) {
                layout += word.whatami() + " ";
            }
            return layout + group(result);
        }
        return "Invalid Entry";
    }

    public static void main(String[] args) {
        launch(args);
    }
}
