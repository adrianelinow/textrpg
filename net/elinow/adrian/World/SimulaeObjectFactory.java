package net.elinow.adrian.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by adrian on 5/9/17.
 */

public class SimulaeObjectFactory {

    public SimulaeObject obj;

    public SimulaeObjectFactory(){}

    public SimulaeObject newObject(HashMap<String, HashMap<String, Object>> map){

        HashMap<String, String> refs = new HashMap<>();
        HashMap<String, Boolean> chks = new HashMap<>();
        HashMap<String, ArrayList<SimulaeObject>> rltv = new HashMap<>();
        HashMap<String, Integer> attr = new HashMap<>();
        HashMap<String, String> dgrp = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> dglg = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> skls = new HashMap<>();

        for(String atr : map.keySet()){
            if(atr.equals("references"))
                for(String k : map.get(atr).keySet())
                    refs.put(k, (String) map.get(atr).get(k));
            if(atr.equals("checks"))
                for(String k : map.get(atr).keySet())
                    chks.put(k, (boolean) map.get(atr).get(k));
            if(atr.equals("relatives"))
                for(String k : map.get(atr).keySet())
                    rltv.put(k, (ArrayList<SimulaeObject>) map.get(atr).get(k));
            if(atr.equals("attributes"))
                for(String k : map.get(atr).keySet())
                    attr.put(k, (int) map.get(atr).get(k));
            if(atr.equals("dialogReplies"))
                for(String k : map.get(atr).keySet())
                    dgrp.put(k,(String) map.get(atr).get(k));
            if(atr.equals("dialogLog"))
                for(String k : map.get(atr).keySet())
                   dglg.put(k, (HashMap<String, Integer>) map.get(atr).get(k));
            if(atr.equals("skillset"))
                for(String k : map.get(atr).keySet())
                    skls.put(k, (HashMap<String, Integer>) map.get(atr).get(k));
        }
        return new SimulaeObject(refs, chks, rltv, attr, dgrp, dglg, skls);
    }

}