package net.elinow.adrian.World;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adrian on 5/4/17.
 */
public class SimulaeObject {

    private HashMap<String, String> references;
    private HashMap<String, Boolean> checks;
    private HashMap<String, ArrayList<SimulaeObject>> relatives;
    private HashMap<String, Integer> attributes;
    private HashMap<String, String> dialogReplies;
    private HashMap<String, HashMap<String, Integer>> dialogLog;
    private HashMap<String, HashMap<String, Integer>> skillset;

    public SimulaeObject(HashMap<String, String> references,
                         HashMap<String, Boolean> checks,
                         HashMap<String, ArrayList<SimulaeObject>> relatives,
                         HashMap<String, Integer> attributes,
                         HashMap<String, String> dialogReplies,
                         HashMap<String, HashMap<String, Integer>> dialogLog,
                         HashMap<String, HashMap<String, Integer>> skillset) {
        this.references = references;
        this.checks = checks;
        this.relatives = relatives;
        this.attributes = attributes;
        this.dialogReplies = dialogReplies;
        this.dialogLog = dialogLog;
        this.skillset = skillset;
    }

    // Dialog Methods //
    public String reply(String topic) {
        if (this.dialogReplies.containsKey(topic) &&
                this.dialogLog.get(topic).get("uses") + 1 <=
                        this.dialogLog.get(topic).get("limit")) {
            this.dialogLog.get(topic).put("uses", this.dialogLog.get(topic).get("uses") + 1);
            return this.dialogReplies.get(topic);
        } else {
            return "I already told you about that";
        }
    }

    public boolean contains(String name){
        for(SimulaeObject obj : this.relatives.get("contents")){
            if(obj.hasReference(name)){
                return true;
            }
        }
        return false;
    }

    // Relatives Methods //
    public boolean relativeTo(String objName, String relative) {
        for (SimulaeObject obj : this.relatives.get(relative))
            if (obj.references.get("objectName").equals(objName))
                return true;
        return false;
    }

    public ArrayList<ArrayList<SimulaeObject>> relatives() {
        ArrayList<ArrayList<SimulaeObject>> ret = new ArrayList<>();
        for (ArrayList<SimulaeObject> list : this.relatives.values())
            ret.add(list);
        return ret;
    }

    public ArrayList<SimulaeObject> getRelatives(String relative) {
        return this.relatives.get(relative);
    }

    // Aspect Methods //
    public boolean isCalled(String name){
        if(this.name("name").equals(name)) return true;
        return false;
    }

    public String name(String nametype) {
        return this.references.get(nametype);
    }

    public boolean hasReference(String ref) {
        for (String s : references.values())
            if (s.equals(ref)) return true;
        return false;
    }

    public int attribute(String attr) {
        return this.attributes.get(attr);
    }

    public boolean check(String checkfor) {
        try {
            if (this.checks.get(checkfor)) return true;
        } catch (NullPointerException npe) {
            return false;
        }
        return false;
    }

    public String toString(){
        return this.toJSON().toString(4);
    }

    public JSONObject relativesToJSON(){
        JSONObject relatives = new JSONObject();

        JSONArray contents = new JSONArray();
        for(SimulaeObject simObj : this.relatives.get("contents"))
            contents.put(simObj.toJSON());

        JSONArray components = new JSONArray();
        for(SimulaeObject simObj : this.relatives.get("components"))
            components.put(simObj.toJSON());

        JSONArray stack = new JSONArray();
        for(SimulaeObject simObj : this.relatives.get("stack"))
            stack.put(simObj.toJSON());

        JSONArray accessories = new JSONArray();
        for(SimulaeObject simObj : this.relatives.get("accessories"))
            accessories.put(simObj.toJSON());

        return relatives
                .put("contents", contents)
                .put("components", components)
                .put("stack", stack)
                .put("accessories", accessories);
    }

    public JSONObject toJSON(){
        JSONObject self = new JSONObject()
                .put("references", this.references)
                .put("checks", this.checks.keySet())
                .put("attributes", this.attributes)
                .put("relatives", relativesToJSON())
                .put("dialogLogs", this.dialogLog)
                .put("dialogReplies", this.dialogReplies)
                .put("skillset", this.skillset);
        return self;
    }

}

/*
 * new SimulaeObject(
        new HashMap<String, String>(){{
            //References
        }},
        new HashMap<String, Boolean>(){{
            //Checks
        }},
        new HashMap<String, ArrayList<SimulaeObject>>(){{
            //Relatives
            put("contents", new ArrayList<SimulaeObject>(){{}});
            put("components", new ArrayList<SimulaeObject>(){{}});
            put("stack", new ArrayList<SimulaeObject>(){{}});
            put("accessories", new ArrayList<SimulaeObject>(){{}});
        }},
        new HashMap<String, Integer>(){{
            //Attributes
        }},
        new HashMap<String, String>(){{
            //DialogReplies
        }},
        new HashMap<String, HashMap<String, Integer>>(){{
            //DialogLog
        }},
        new HashMap<String, HashMap<String, Integer>>(){{
            //Skillset
        }}
   );
 */