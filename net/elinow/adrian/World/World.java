package net.elinow.adrian.World;

import org.json.JSONObject;

import java.io.FileWriter;
import java.util.HashMap;

/**
 * Created by adrian on 5/3/17.
 */
public class World {

    private HashMap<String, SimulaeObject> zones;
    public SimulaeObject currentLocation;

    public World(HashMap<String, SimulaeObject> zones){
        this.zones = zones;
    }

    public SimulaeObject findPlayer(String playername){
        for(SimulaeObject zone : this.zones.values()){
            for(SimulaeObject location : zone.getRelatives("contents")){
                if(location.contains(playername)){
                    return location;
                }
            }
        }
        return null;
    }

    public void printSelf() {
        try {
            JSONObject world = new JSONObject();
            for (String k : this.zones.keySet())
                world.put(k, this.zones.get(k).toJSON());
            System.out.println(world.toString());
            world.write(new FileWriter("World.json")).close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
