package net.elinow.adrian.World;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by adrian on 5/10/17.
 */
public class Loader {

    private JSONTokener tokener;
    private JSONObject object;
    private BufferedReader br;

    public Loader(String filename) {
        try {
            br = new BufferedReader(new FileReader(filename));
            tokener = new JSONTokener(br);
            object = new JSONObject(tokener);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private HashMap<String, Boolean> loadChecks(Object raw){
        HashMap<String, Boolean> checks = new HashMap<>();
        List<Object> mapped = (new JSONArray(raw.toString().replace('=',':'))).toList();
        for(Object k : mapped)
            checks.put(k.toString(), true);
        return checks;
    }

    private HashMap<String, String> loadReferences(Object raw){
        HashMap<String, String> refs = new HashMap<>();
        Map<String, Object> mapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();
        for(String k : mapped.keySet())
            refs.put(k, mapped.get(k).toString());
        return refs;
    }

    private HashMap<String, ArrayList<SimulaeObject>> loadRelatives(Object raw){
        HashMap<String, ArrayList<SimulaeObject>> relatives = new HashMap<>();
        Map<String, Object> mapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();
        for(String k : mapped.keySet()){
            relatives.put(k, loadRelative(mapped.get(k)));
        }
        return relatives;
    }

    private ArrayList<SimulaeObject> loadRelative(Object raw){
        ArrayList<SimulaeObject> full = new ArrayList<>();
        List<Object> list = (new JSONArray(raw.toString().replace('=',':'))).toList();
        for(Object o : list){
            full.add(loadSimulaeObject(o));
        }
        return full;
    }

    private HashMap<String, HashMap<String, Integer>> loadDoubleMap(Object raw){
        HashMap<String, HashMap<String, Integer>> logs = new HashMap<>();
        Map<String, Object> halfMapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();
        for(String k : halfMapped.keySet())
            logs.put(k, loadAttributes(halfMapped.get(k)));
        return logs;
    }

    private HashMap<String, String> loadDialogReplies(Object raw){
        HashMap<String, String> replies = new HashMap<>();
        Map<String, Object> mapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();
        for(String k : mapped.keySet())
            replies.put(k, mapped.get(k).toString());
        return replies;
    }

    private HashMap<String, Integer> loadAttributes(Object raw){
        HashMap<String, Integer> attributes = new HashMap<>();
        Map<String, Object> mapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();
        for(String k : mapped.keySet())
            attributes.put(k, Integer.parseInt(mapped.get(k).toString()));
        return attributes;
    }

    private SimulaeObject loadSimulaeObject(Object raw){
        Map<String, Object> mapped = (new JSONObject(raw.toString().replace('=',':'))).toMap();

        HashMap<String, String> references = new HashMap<>();
        HashMap<String, Boolean> checks = new HashMap<>();
        HashMap<String, ArrayList<SimulaeObject>> relatives = new HashMap<>();
        HashMap<String, Integer> attributes = new HashMap<>();
        HashMap<String, String> dialogReplies = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> dialogLog = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> skillset = new HashMap<>();
        for(String a : mapped.keySet()){
            if(a.equals("references")){
                references = loadReferences(mapped.get(a));
           }else if(a.equals("checks")){
                checks = loadChecks(mapped.get(a));
           }else if(a.equals("attributes")){
                attributes = loadAttributes(mapped.get(a));
           }else if(a.equals("relatives")){
                relatives = loadRelatives(mapped.get(a));
           }else if(a.equals("dialogReplies")){
                dialogReplies = loadDialogReplies(mapped.get(a));
           }else if(a.equals("dialogLogs")){
                dialogLog = loadDoubleMap(mapped.get(a));
           }else if(a.equals("skillset")){
                skillset = loadDoubleMap(mapped.get(a));
           }
        }

        return new SimulaeObject(
                references,
                checks,
                relatives,
                attributes,
                dialogReplies,
                dialogLog,
                skillset
        );
    }

    public World load(){
        Map<String, Object> map = object.toMap();
        HashMap<String, SimulaeObject> world = new HashMap<String, SimulaeObject>();
        for(String key : map.keySet()){
            world.put(key, loadSimulaeObject(map.get(key)));
        }
        return new World(world);
    }

}
