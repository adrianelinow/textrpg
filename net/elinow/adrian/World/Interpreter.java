package net.elinow.adrian.World;

import java.util.ArrayList;

import net.elinow.adrian.Parser.Noun;
import net.elinow.adrian.Parser.PartOfSpeech;
import net.elinow.adrian.Parser.Parser;

/**
 * Created by adrian on 5/9/17.
 */
public class Interpreter {

    private World construct;
    private Parser parser;

    public Interpreter(World construct, Parser parser){
        this.construct = construct;
        initLocation();
        this.parser = parser;
    }

    public void initLocation(){
        construct.currentLocation = construct.findPlayer("Player");
        if(construct.currentLocation == null){
            System.out.println("[CRITICAL GAME ERROR] : COULD NOT FIND PLAYER");
            throw new NullPointerException();
        }

    }

    public SimulaeObject getReference(String name, SimulaeObject location){
        for(ArrayList<SimulaeObject> relative : location.relatives())
            for(SimulaeObject so : relative)
                if(so.hasReference(name)) return so;
        return null;
    }

    public String parse(String command){
        ArrayList<ArrayList<PartOfSpeech>> grouping = this.parser.groupStatements(this.parser.tokenize(command));

        for(ArrayList<PartOfSpeech> statement : grouping){

            for(PartOfSpeech word : statement){
                if(word instanceof Noun){
                    if(construct.currentLocation.contains(word.getValue())){
                        System.out.println("Is Noun AND Has Reference");
                    }else{
                        System.out.println("No Reference Found");
                    }
                }else{
                    System.out.println("Is NOT a Noun");
                }
            }
        }

        String ret = "";
        for(ArrayList<PartOfSpeech> state : grouping){
            for(PartOfSpeech pos : state){
                ret += pos.getValue()+" - "+pos.whatami()+", ";
            }
            ret += "\n";
        }

        return ret;
    }

}
